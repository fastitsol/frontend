import Vue from 'vue'
import iView from 'iview'
import locale from 'iview/dist/locale/en-US' // Change locale, check node_modules/iview/dist/locale
import VueLuxon from "vue-luxon";
Vue.use(VueLuxon);
Vue.use(iView, {
  locale
})


// mixins for using common methods

Vue.mixin({

  methods: {
      validationError(res){
        for(let e of res.data.errors){
            this.i(e.message)
        }
      },
      i(msg, i = 'Hey!') {
          this.$Notice.info({
              title: i,
              desc: msg,

          });
      },
      s(msg, i = 'Great!') {
          this.$Notice.success({
              title: i,
              desc: msg,

          });
      },
      w(msg, i = 'Hi!') {
          this.$Notice.warning({
              title: i,
              desc: msg,
              duration: 0

          });
      },
      e(msg, i = 'Oops!') {
          this.$Notice.error({
              title: i,
              desc: msg,

          });
      },
      swr() {
          this.$Notice.error({
              title: 'Oops',
              desc: 'Something went wrong, please try again later'
          });
      },
      async callApi(method, url, dataObj) {
          try {
              let data = await this.$axios({
                  config: {
                    withCredentials: false,

                  },
                  method: method,
                  url: url,
                  data: dataObj,

              })
              return data
          } catch (e) {
              let res = e.response
              if(res.status == 403){
                  window.location = '/login'
              }else if(res.status == 422){
                 this.validationError(res)

              }else if(res.status == 404 || res.status == 401){
                 this.i(res.data.msg)
              }else{
                 this.swr()
              }
              return e.response
          }
      },
      showVError(data){
        for(let e of data){
          this.i(e.message)
        }
      }


  }
})

