import Vue from 'vue'
import * as tus from 'tus-js-client';
import VueFileAgent, { plugins } from 'vue-file-agent'
import VueFileAgentStyles from 'vue-file-agent/dist/vue-file-agent.css';
plugins.tus = tus
VueFileAgent.plugins.tus = tus

Vue.use(VueFileAgent)
