//import Vuex from 'vuex'
//import axios from 'axios'
import Vue from 'vue';
export const strict = false
export const state = () => ({
    user : 'no user',
    authUser: false,
    feeds : []
})
// common getters
export const getters ={
    getFeeds(state){
      return state.feeds
    }
}
//mutations for changing data from action
export const mutations = {
  loginUser(state, data) {
    state.authUser = data
  },
  setFeed(state,data){
    state.feeds = data
  },
  setNewSingleFeed(state,data){
    state.feeds.unshift(data)
  },
  // for feed post likes
  setLikeCounter(state, data){
    let feedIndex = state.feeds.findIndex( f => f.id == data.feed_id)
    data.isLike ? state.feeds[feedIndex].like_count++ : state.feeds[feedIndex].like_count--

  },
  setLikedOrNotLiked(state, data){
    console.log('setLikedOrNotLiked', data)
    let feedIndex = state.feeds.findIndex( f => f.id == data.feed_id)
    state.feeds[feedIndex].like = data.isLike ? data : null
    console.log(state.feeds[feedIndex])
  },




  // add comments
  pushComment(state, data){
    for(let d of state.feeds){
       if(d.id == data.feed_id){
         d.comments = [data]
         console.log(d)
       }
    }
    // let feedIndex = state.feeds.findIndex( f => f.id == data.feed_id)

    // console.log(state.feeds[feedIndex].comments)
    // Vue.set(state.feeds[feedIndex].comments,0,data);
    // for(let d of state.feeds[feedIndex].comments){
    //    Vue.set(d, 0, data)
    //    //d.push(data)
    // }
    //state.feeds[feedIndex].comments.unshift(data)

  },
  // remove comment like
  removeCommentLike(state, likeObj){
    let feedIndex = state.feeds.findIndex( f => f.id == likeObj.feed_id)
    console.log('feed is', state.feeds[feedIndex])
    let commentIndex = state.feeds[feedIndex].comments.findIndex( c => c.id == likeObj.comment_id)
    console.log('comment is', state.feeds[feedIndex].comments[commentIndex])
    state.feeds[feedIndex].comments[commentIndex].commentlike = null
    state.feeds[feedIndex].comments[commentIndex].like_count--

  },
  // add comment like
  addCommentLike(state, likeObj){
    let feedIndex = state.feeds.findIndex( f => f.id == likeObj.feed_id)
    console.log('feed is', state.feeds[feedIndex])
    let commentIndex = state.feeds[feedIndex].comments.findIndex( c => c.id == likeObj.comment_id)
    console.log('comment is', state.feeds[feedIndex].comments[commentIndex])
    state.feeds[feedIndex].comments[commentIndex].commentlike = likeObj
    state.feeds[feedIndex].comments[commentIndex].like_count++

  },


  // add reply with comment id as parrent_id
  pushReply(state, data){
    let feedIndex = state.feeds.findIndex( f => f.id == data.feed_id)
    let comments = state.feeds[feedIndex].comments
    let commentIndex = comments.findIndex( c => c.id == data.parrent_id)
    state.feeds[feedIndex].comments[commentIndex].replies.push(data)
  },
  increaseCommentCounter(state, data){
    let feedIndex = state.feeds.findIndex( f => f.id == data.feed_id)
    state.feeds[feedIndex].comment_count++
  },
  decreaseCommentCounter(state, data){
    let feedIndex = state.feeds.findIndex( f => f.id == data.feed_id)
    state.feeds[feedIndex].comment_count--
  },
  setComments(state, data){ // adding new comments items
    let feedIndex = state.feeds.findIndex( f => f.id == data[0].feed_id)
    for(let d of data){
      state.feeds[feedIndex].comments.push(d)
    }

  },


}
// actionns for commiting mutations
export const actions = {
  async nuxtServerInit({ commit }, { $axios }) {
   try {
      // get the initial data
      let { data } = await $axios.get('auth/getUser')
      commit('loginUser', data)


    } catch (e) {
        console.log('an error occured in nuxt init')
    }
  },
}
